/*import React from 'react';*/
import './App.css';
import Loading from './Loading';
import React, {useState,useEffect} from 'react';

/*vídeo usado pra referência: https://www.youtube.com/watch?v=A3VPjS_OPvY */

const App = () => {
  const [cat,setCat]= useState('');
  const [done,setDone] = useState(undefined);
  const [loading,setLoading] = useState(undefined);
  

  useEffect(() => {
    getCat();
  },[]);

  const getCat = () =>{
    setDone(true);
    /*set as undefined on the vídeo before(21:30) */
    
    setTimeout(()=>{
      fetch('http://aws.random.cat/meow')
      .then(res => res.json())
      .then(data=> {
        setCat(data.file);
        setLoading(true);
        setTimeout(()=>{setDone(true);})
      },1000);
    },1200);
  }

  return (
    <div className="App">

      <h3>Get Random Cat Image</h3>

      <button onClick={getCat}>Get Cat</button>

      <div className="cat-pic">
        {!done ? (
          <Loading loading={loading}/>
        ) : (
          <img className="catImage" src={cat} alt="Cat" />
        )}
      </div>
    </div>
  );
}

export default App;
